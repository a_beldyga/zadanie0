﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Koszykowka : Sport
    {
        private string nazwaDruzyny;
        public Koszykowka() :base()
        { }
        public Koszykowka(int liczbaZawodnikowZ, int czasGryZ, string nazwaDruzyny) : base(liczbaZawodnikowZ, czasGryZ)
        {
            this.nazwaDruzyny = nazwaDruzyny;
        }


       public override string PodajSklad()
        {

            return "Koszykówka: "+base.PodajSklad() + ", a nazwa druzyny to: " + nazwaDruzyny;
        }

       
    }
}
