﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Sport
    {
        private int liczbaZawodnikow;
        public int LiczbaZawodnikow
        {
            get
            {
                return liczbaZawodnikow;
            }
            set
            {
                liczbaZawodnikow = value;

            }
        }

        private int czasGry;
        public int CzasGry
        {
            get
            {
                return czasGry;
            }
            set
            {

                czasGry = value;
            }
        }

        public Sport() { }

        public Sport(int liczbaZawodnikowZ, int czasGryZ)
        {
            this.liczbaZawodnikow = liczbaZawodnikowZ;
            this.czasGry = czasGryZ;
        }

        public virtual string PodajSklad()
        {
            string napis = "ilosc zawodnikow to:" + liczbaZawodnikow;

            return napis;
        }

        public override string ToString()
        {
            return "ilość zawodników" + liczbaZawodnikow;
        }

    }
}
