﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Sport);
        public static Type B = typeof(Siatkowka);
        public static Type C = typeof(Koszykowka);

        public static string commonMethodName = "PodajSklad";
    }
}
