﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Siatkowka zespol1 = new Siatkowka(6, 30, true);
            Koszykowka zespol2 = new Koszykowka(5, 45, "Rangers");

            Console.WriteLine(zespol1.PodajSklad());
            Console.WriteLine(zespol2.PodajSklad());

            Console.ReadKey();

        }
    }
}
