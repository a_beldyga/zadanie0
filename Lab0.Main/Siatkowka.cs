﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Siatkowka : Sport
    {

        private bool zagrywajacy;

        public Siatkowka(): base(){ }


        public Siatkowka(int liczbaZawodnikowZ, int czasGryZ, bool zagrywajacy) : base(liczbaZawodnikowZ, czasGryZ)
        { this.zagrywajacy = true; }

        public override string PodajSklad()
        {
            string napis;
   
            if (zagrywajacy==true)
	{
                 napis = "wystepuje";
		 
	}
            else{  napis= "nie wystepuje";}
 
            return "SIatkówka: "+base.PodajSklad()+", "+napis+" w tej grze zagrywający, a czas gry to: "+CzasGry+" minut" ;
                            
        }   

    }
}
